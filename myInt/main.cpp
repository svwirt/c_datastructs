//
//  main.cpp
//  7a_Project
//
//  Created by Stephanie Ayala on 2/17/18.
//  Copyright © 2018 Stephanie Ayala. All rights reserved.
//
#ifndef MYINTEGER_HPP
#define MYINTEGER_HPP

#include <iostream>
using namespace std;

class MyInteger
{
private:
    int *pInteger;
public:
    MyInteger(int);
    MyInteger();
    MyInteger& operator=(const MyInteger &);
    MyInteger(const MyInteger &);
    ~MyInteger();
    void setMyInt(int);
    int getMyInt() const;
};
#endif
MyInteger::MyInteger(int obj)
{
    pInteger = new int;
    *pInteger = obj;
}
MyInteger::MyInteger()
{
    pInteger = new int;
    *pInteger = 0;
}
MyInteger& MyInteger::operator=(const MyInteger &obj)
{
    pInteger = new int;
    *pInteger = *obj.pInteger;
    return *this;
   
}
MyInteger::MyInteger(const MyInteger &obj)
{
    pInteger = new int;
    *pInteger = obj.getMyInt();
}
MyInteger::~MyInteger()
{

    delete pInteger;
}

void MyInteger::setMyInt(int num)
{
    *pInteger = num;
}

int MyInteger::getMyInt() const
{
    return *pInteger;
}

int main(int argc, const char * argv[]) {
    /*MyInteger obj1(17);
    MyInteger obj2 = obj1;
    std::cout << obj1.getMyInt() << std::endl;
    std::cout << obj2.getMyInt() << std::endl;
    
    obj2.setMyInt(9);
    std::cout << obj1.getMyInt() << std::endl;
    std::cout << obj2.getMyInt() << std::endl;
    
    MyInteger obj3(42);
    obj2 = obj3;
    std::cout << obj2.getMyInt() << std::endl;
    std::cout << obj3.getMyInt() << std::endl;
    
    obj3.setMyInt(1);
    std::cout << obj2.getMyInt() << std::endl;
    std::cout << obj3.getMyInt() << std::endl;
    
    obj1.setMyInt(0);
    obj3 = obj2 = obj1;
    std::cout << obj1.getMyInt() << std::endl;
    std::cout << obj2.getMyInt() << std::endl;
    std::cout << obj3.getMyInt() << std::endl;*/

    MyInteger obj4;
    std::cout << obj4.getMyInt() << std::endl;
    return 0;


}

