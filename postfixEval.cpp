/********************************************************************************************************************************
 * Author: Stephanie Ayala
 * Date: 03-05-2018
 * Description: this program uses a function called postfixEval that ecaluates a postfix expression. It takes a C-style strisng as a parameter and and uses helper functions to ecaluate the expression.
 ******************************************************************************************************************************/

#include <iostream>
#include <stack>
#include <cstdlib>

using namespace std;

double postfixEval(char []);
double operate(double, double, char);
double digits(char [], int &);

/*
int main()
{
    char exp[] = "25 12 7 - 2 * /";
    cout << postfixEval(exp);
    return 0;
}
*/

double postfixEval(char exp[])
{
    //declare a stack of doubles
    stack<double> myStack;
    double num1, num2, result;

    // when i has not reached the null terminator
    for(int i=0; exp[i] != '\0'; i++)
    {
 	// if the character is a space
 	if(exp[i] == ' ')
	{
		continue;
	}
	
	// if the char is a number
	else if(isdigit(exp[i]))
	{
		result = digits(exp, i);
	}
	
	// if the char is an operator
	else
	{
		// pop two numbers off the stack and set them to num2 and num1
		num2 = myStack.top();
		myStack.pop();
		num1 = myStack.top();
		myStack.pop();
	
		// call the function to do the math
		result = operate(num1, num2, exp[i]);
	}
	
	// push the result onto the stack
	myStack.push(result);
    }
    // when it is down to one, set it to be the result, clear the stack and return the result
    result = myStack.top();
    myStack.pop();
    return result;
}

// this function accepts the expression and the individial digit by reference
// if there are two numbers in a row that belong together
// it multiplys the first digit times 10 and adds the second
double digits(char exp[], int &i)
{
    double number = 0;
    double n;
    while(isdigit(exp[i]))
    {
    	n = (int)(exp[i] - '0');
	number = (number * 10) + n;
	i++;
    }
    i--;
    return number;
}

//This function takes in two digits and an operator character as parameters
//It uses a switch statement to determine which operator to use
//and returns a different evaluation for each case
double operate(double num1, double num2, char obj)
{
    switch(obj)
    {
    case '+':
	return (num1 + num2);
    case '-':
	return (num1 - num2);
    case '*':
	return (num1 * num2);
    case '/':
	return (num1 / num2);
    default:
	return -1;
    }
}

