#include <iostream>
#include <cmath>
#include <vector>
#include <memory>
using namespace std;
/*******************************************************************************************
 Shape class
 *******************************************************************************************/
class Shape
{
    
public:
    Shape(){}
    virtual double area() = 0;
    virtual double perimeter() = 0;
    
};

/*******************************************************************************************
 Circle class
 *******************************************************************************************/
class Circle : public Shape
{
private:
    double radius;
public:
    Circle();
    Circle(double);
    double area();
    double perimeter();
    void setRadius(double);
    
};

Circle::Circle()
{
    this->radius= 0;
}

Circle::Circle(double radiusIn)
{
    this->radius= radiusIn;
}
void Circle::setRadius(double radiusIn)
{
    this->radius = radiusIn;
}
double Circle::area()//override
{
    return 3.14159 * pow(radius, 2);
}
double Circle::perimeter()//override
{
    return 2 * 3.14159 * radius;
    
}
/*******************************************************************************************
 Rectangle class
 *******************************************************************************************/
class Rectangle : public Shape
{
protected:
    double length;
    double width;
public:
    Rectangle();
    Rectangle(double, double);
    virtual void setLength(double);
    virtual void setWidth(double);
    double area();
    double perimeter();
    
};

Rectangle::Rectangle()
{
    this->length = 0;
    this->width = 0;
}

Rectangle::Rectangle(double lengthIn, double widthIn)
{
    setLength(lengthIn);//this->length = lengthIn;
    setWidth(widthIn);//this->width = widthIn;
}
void Rectangle::setLength(double lengthIn)
{
    this->length = lengthIn;
}
void Rectangle::setWidth(double widthIn)
{
    this->width = widthIn;
}
double Rectangle::area()
{
    return length * width;
}
double Rectangle::perimeter()
{
    return 2 * length + 2 * width;
    
}

/*******************************************************************************************
 Square class
 *******************************************************************************************/
class Square : public Rectangle
{
private:
    double side;
public:
    Square(): Rectangle(0, 0){}
    Square(double side) : Rectangle(side, side){}
    void setLength(double);
    void setWidth(double);
};

/*Square::Square() : Rectangle(0, 0)
 {
 }
 Square::Square(double side) : Rectangle(side, side)
 {
 }*/
void Square::setLength(double sideIn)
{
    this->length = sideIn;
    this->width = sideIn;//setWidth(sideIn);
}
void Square::setWidth(double sideIn)
{
    this->width=sideIn;
    this->length = sideIn;//setLength(sideIn);
}

double averageArea(vector<Shape*>);


int main(int argc, const char * argv[]) {
    
    Shape *c1 = new Circle(3.4);
    Shape *c2 = new Circle(2.8);
    Shape *r1= new Rectangle(2, 4);
    Shape *r2= new Rectangle(3.4, 4);
    Shape *s1= new Square(1.2);
    Shape *s2= new Square(2.3);
    
    cout << "circle1 area " << c1->area() << endl;
    cout << "circle1 perimeter " << c1->perimeter() << endl;
    cout << "circle2 area " << c2->area() << endl;
    cout << "circle2 perimeter " << c2->perimeter() << endl;
    cout << "Rectangle1 area " << r1->area() << endl;
    cout << "Rectangle1 perimeter " << r1->perimeter() << endl;
    cout << "Rectangle2 area " << r2->area() << endl;
    cout << "Rectangle2 perimeter " << r2->perimeter() << endl;
    cout << "Square1 area " << r1->area() << endl;
    cout << "Square1 perimeter " << r1->perimeter() << endl;
    cout << "Square2 area " << r2->area() << endl;
    cout << "Square2 perimeter " << r2->perimeter() << endl;
    
    
    vector<Shape*>vectIn;
    vectIn.push_back(c1);
    vectIn.push_back(c2);
    vectIn.push_back(r1);
    vectIn.push_back(r2);
    vectIn.push_back(s1);
    vectIn.push_back(s2);
    cout << "average area is " << averageArea(vectIn) << endl;
    return 0;
}

double averageArea(vector<Shape*> shapeVect)
{
    double sum = 0;
    for(int i=0; i < shapeVect.size(); i++)
    {
        sum += shapeVect[i]->area();
    }
    return sum/shapeVect.size();
}


